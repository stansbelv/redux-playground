import "./App.css";
import { PostContainer } from "./components/PostContainer";
import { UserContainer } from "./components/UserContainer";

function App() {
  return (
    <div className="App">
      <div style={{ display: "flex" }}>
        <PostContainer />
        <PostContainer />
      </div>
      <div style={{ display: "flex" }}>
        <UserContainer />
      </div>
    </div>
  );
}

export default App;
