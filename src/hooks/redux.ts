import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "../store/store"

//Typed dispatch hook
const useAppDispatch = () => {
    return useDispatch<AppDispatch>()
}

//Typed selector hook
const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export {useAppDispatch, useAppSelector}