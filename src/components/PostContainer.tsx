import React from "react";
import { IPost } from "../models/IPost";
import { postAPI } from "../services/PostService";
import { PostItem } from "./PostItem";

export const PostContainer = () => {
  const {
    data: posts,
    error,
    isLoading,
    refetch,
    // Generated RTK query hook
  } = postAPI.useFetchAllPostsQuery(
    15
    // { pollingInterval: 1000 }
  );

  const [createPost, {}] = postAPI.useCreatePostMutation();
  const [updatePost, {}] = postAPI.useUpdatePostMutation();
  const [deletePost, {}] = postAPI.useDeletePostMutation();

  const handleCreate = async () => {
    const title = prompt();
    await createPost({
      title,
      body: title,
    } as IPost);
  };

  const handleRemove = (post: IPost) => {
    deletePost(post);
  };
  const handleUpdate = (post: IPost) => {
    updatePost(post);
  };

  if (error) {
    return <h1>Error</h1>;
  }

  return (
    <div>
      {isLoading && <h1>Is Loading...</h1>}
      <button onClick={handleCreate}>create</button>

      <button onClick={() => refetch()}>refetch</button>

      {posts?.map((p) => (
        <PostItem
          post={p}
          key={p.id}
          remove={handleRemove}
          update={handleUpdate}
        >
          {p.title}
        </PostItem>
      ))}
    </div>
  );
};
