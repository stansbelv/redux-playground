import React, { useEffect } from "react";

import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { fetchUsers } from "../store/reducers/ActionCreators";

export const UserContainer: React.FC = () => {
  const { users, isLoading, error } = useAppSelector(
    (state) => state.userReducer
  );

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  if (error) {
    return <h1>{error}</h1>;
  }

  return (
    <div>
      {isLoading && <h1>Loadings</h1>}
      {users.map((u) => (
        <p key={u.id}>{u.name}</p>
      ))}
    </div>
  );
};
